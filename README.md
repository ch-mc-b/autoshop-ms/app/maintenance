Wartung Services
================

[[_TOC_]]

Verarbeiten Maschinendaten via
* MQTT - mosquitto - REST
* REST

Leitet diese weiter an
* Maschine Learning - Prediktive Maintance
* Auslösen Service Prozess, z.B. ein BPMN Workflow mit Camunda.


